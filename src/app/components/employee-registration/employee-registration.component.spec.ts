import { ComponentFixture, fakeAsync, inject, TestBed, async, tick } from '@angular/core/testing';

import { EmployeeRegistrationComponent } from './employee-registration.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { AuthService } from  '../../auth/auth.service';


const mockEmplyeeData: any = {
  firstName: "Neha",
  lastName: "Goswami",
  gender: "female",
  dob: "1992-03-24",
  department: "UI developer"
}

describe('EmployeeRegistrationComponent', () => {
  let component: EmployeeRegistrationComponent;
  let fixture: ComponentFixture<EmployeeRegistrationComponent>;
  let router: Router;
  let employeeService: EmployeeService;
  let authService: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeRegistrationComponent ],
      imports: [ ReactiveFormsModule, HttpClientTestingModule, RouterTestingModule ],
      providers: [EmployeeService, AuthService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeRegistrationComponent);
    component = fixture.componentInstance;
    employeeService = TestBed.get(EmployeeService); 
    authService = TestBed.get(AuthService); 
    router = TestBed.get(Router);
    
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check form not valid', () => {
    expect(component.registrationForm.valid).toBeFalsy();
  });

  it('should initialise initailiseRegistrationForm', () => {
    component.registrationForm = undefined;
    component.initailiseRegistrationForm();
    expect(component.registrationForm).toBeTruthy();
  });

  it('should check if registrationForm is valid and saveEmployeeDetails have been called', () => {
    expect(component.registrationForm.valid).toBeFalsy();
    const spyService = spyOn(employeeService, 'saveEmployeeDetails').and.callThrough()


    component.registrationForm.controls['firstName'].setValue('Neha');
    component.registrationForm.controls['lastName'].setValue('Goswami');
    component.registrationForm.controls['gender'].setValue('female');
    component.registrationForm.controls['dob'].setValue('1992-03-24');
    component.registrationForm.controls['department'].setValue('UI developer');

    expect(component.registrationForm.valid).toBeTruthy();
    component.onSubmit();
    expect(spyService).toBeDefined();
    expect(spyService).toHaveBeenCalled();


  });
  
  it('should change the value on selection change', async(() => {
    fixture.detectChanges();
    let select: HTMLSelectElement = fixture.debugElement.query(By.css('.custom-select')).nativeElement;
    select.value = select.options[2].value;
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      let text = select.options[select.selectedIndex].label;
      expect(text).toBe('Data science');
    });
  })); 

  it('should check first name invalid', () => {
    let firstName = component.registrationForm.controls['firstName'];
    expect(firstName.valid).toBeFalsy();
    expect(firstName.errors['required']).toBeTruthy;
  });

  it('should call onSubmit method', fakeAsync (() => {
    spyOn(component, 'onSubmit').and.callThrough();
    const onSubmit = fixture.debugElement.query(By.css('.btn')).nativeElement;
    onSubmit.click();
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.onSubmit).toHaveBeenCalled();
      expect(component.submitted).toBeTruthy();
    })
  }));

  it('should check first name invalid', () => { 
    expect(authService instanceof AuthService).toBeTruthy();
  });

});
