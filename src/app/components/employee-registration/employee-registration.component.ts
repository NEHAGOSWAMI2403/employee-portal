import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from  '@angular/router';
import { Subscription } from 'rxjs';
import { EmployeeService } from 'src/app/services/employee.service';
import { AuthService } from  '../../auth/auth.service';

@Component({
  selector: 'app-employee-registration',
  templateUrl: './employee-registration.component.html',
  styleUrls: ['./employee-registration.component.scss']
})
export class EmployeeRegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  submitted  =  false;
  private sub: Subscription;
  departments: any = ['UI developer', 'Data science', 'Backend Developer', 'Android/Ios Developer', 'Product manager'];

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private employeeService: EmployeeService, private router: Router,
    private authService: AuthService) {
}

  ngOnInit(): void {
    this.initailiseRegistrationForm();  
  }

  initailiseRegistrationForm(){
    this.registrationForm  =  this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      lastName: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', [Validators.required, Validators.pattern(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/)]],
      department: ['', Validators.required],
  }); 
  }

  get formControls() { return this.registrationForm.controls; }

  onSubmit(){
    this.submitted = true;
    
    if (this.registrationForm.valid) {
    this.employeeService.saveEmployeeDetails(this.registrationForm.value).subscribe(data => {
      this.authService.register(this.registrationForm.value);
      this.router.navigateByUrl('/employee-details');
    },
    error => {
      console.error('err', error);
    }
    )
  }
  
}

  changeDepartment(department: any) {
    this.registrationForm.get('department')?.setValue(department.target.value, {
        onlySelf: true
      })
   }

  ngOnDestroy(){
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

}
