import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { EmployeeDetailsComponent } from './employee-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { EmployeeService } from 'src/app/services/employee.service';
import { of, throwError } from 'rxjs';

const emplyeeData: any = {
  firstName: "Neha",
  lastName: "Goswami"
}
describe('EmployeeDetailsComponent', () => {
  let component: EmployeeDetailsComponent;
  let fixture: ComponentFixture<EmployeeDetailsComponent>;
  let employeeService: EmployeeService;
  let mockEmplyeeData = emplyeeData;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeDetailsComponent ],
      imports: [ HttpClientTestingModule, RouterTestingModule ],
      providers: [EmployeeService]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDetailsComponent);
    component = fixture.componentInstance;
    employeeService = TestBed.get(EmployeeService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getEmployeeDetails service', fakeAsync (() => {
    const spy = spyOn(employeeService, 'getEmployeeDetails').and.returnValue(of(mockEmplyeeData));
    const subSpy = spyOn(employeeService.getEmployeeDetails(), 'subscribe');
    component.ngOnInit();
    tick();
    expect(spy).toHaveBeenCalledBefore(subSpy);
    expect(subSpy).toHaveBeenCalled();
  }));

  it('should call subscribe service', fakeAsync (() => {
    component.loading = true;
    const spy = spyOn(employeeService, 'getEmployeeDetails').and.returnValue(throwError({status: 500}))
    component.ngOnInit();
    expect(component.loading).toBeFalsy();
    expect(component.employeeDetails).toBeUndefined();
  }));

  it('should call subscribe ', fakeAsync (() => {
    component.loading = true;
    spyOn(employeeService, 'getEmployeeDetails').and.returnValue(of(mockEmplyeeData));
    component.employeeDetails = mockEmplyeeData;
    component.ngOnInit();
    expect(component.loading).toBeFalsy();
    expect(component.employeeDetails).toBeDefined();
    expect(component.employeeDetails.length).toBeGreaterThan(1);
  }));
});
