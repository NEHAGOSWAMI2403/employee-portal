import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employees } from  '../../models/employee';
import { map, finalize } from 'rxjs/operators'
import { AuthService } from  '../../auth/auth.service';
import { Router } from  '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {
  employeeDetails: Array<Employees>;
  page = 1;
  pageSize = 6;
  pagiDisabled: boolean;
  collectionSize: number;
  loading: boolean;
  private sub: Subscription;

  constructor(private http: HttpClient, private employeeService: EmployeeService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.getEmployeeData();
  }

  getEmployeeData(){
    this.loading = true;
    this.employeeService.getEmployeeDetails()
    .pipe(
      map (resData => {
      const employeeData = [];
      Object.keys(resData).map(index => employeeData.push(resData[index]));
      employeeData.sort((a,b)=> (a.firstName < b.firstName ? -1 : 1))
      return employeeData;
    }),
    finalize(() => (this.loading = false))
    )
    .subscribe(employees => {
        this.employeeDetails = employees;
        this.collectionSize = this.employeeDetails.length;
        if(this.employeeDetails.length < 5)
          this.pagiDisabled = true;

    },
    error => {
      console.error('err', error);
    }
    )
  }

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/registration');
  }

  ngOnDestroy(){
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
}
