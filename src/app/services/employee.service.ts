import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Employees } from  '../models/employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  globalApiUrl = 'https://employee-portal-d649e-default-rtdb.firebaseio.com/';

  constructor(private http: HttpClient) { }

  saveEmployeeDetails(payload: Employees): Observable<any> {
    const url = `${this.globalApiUrl}employee.json`;
    return this.http.post(url, payload)
  }

  getEmployeeDetails(): Observable<Employees> {
    const getUrl = `${this.globalApiUrl}employee.json`;
    return this.http.get<Employees>(getUrl);
  }
}
