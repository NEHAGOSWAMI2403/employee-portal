import { TestBed } from '@angular/core/testing';

import { EmployeeService } from './employee.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { HttpClientModule } from '@angular/common/http'

describe('EmployeeService', () => {
  let service: EmployeeService;
  let httpTestCtrl: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [EmployeeService]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(EmployeeService);
    httpTestCtrl = TestBed.inject(HttpTestingController);
  });

  afterEach(() => httpTestCtrl.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should use http getEmployeeDetails method', () => {
    const url = service.globalApiUrl + 'employee.json';
    const mockGetEmployeeData = 
      { firstName: "Neha", lastName: "Goswami", gender: "female", dob: "1992-03-24", department: "UI developer"};

    service.getEmployeeDetails().subscribe(res => {
      expect(res).toEqual(mockGetEmployeeData);
    });
    const req = httpTestCtrl.expectOne(url)
    expect(req.request.method).toBe('GET')
    req.flush(mockGetEmployeeData);
  });

  it('should be post saveEmployeeDetails', () => {
    const url = service.globalApiUrl + 'employee.json';
    const mockPostEMployeeData = 
      { firstName: "Neha", lastName: "Goswami", gender: "female", dob: "1992-03-24", department: "UI developer"};

    service.saveEmployeeDetails(mockPostEMployeeData).subscribe(res => {
      expect(res).toEqual(mockPostEMployeeData);
    });
    const req = httpTestCtrl.expectOne(url)
    expect(req.request.method).toBe('POST')
    req.flush(mockPostEMployeeData);
  });

});
