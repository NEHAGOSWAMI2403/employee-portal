export class Employees{
    firstName: string;
    lastName: string;
    gender: string;
    dob: string;
    department: string;
}