import { TestBed, async } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';


describe('AuthGuard', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
        providers: [ AuthGuard, AuthService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  describe('canActivate', () => {
    let authGuard: AuthGuard;
    let authService;

    it('should return true for a logged in user', () => {
      authService = { isRegistered: () => true };
      authGuard = new AuthGuard(authService);

      expect(authGuard.canActivate()).toEqual(true);
    });

    it('should navigate to home for a logged out user', () => {
      authService = { isRegistered: () => false };
      authGuard = new AuthGuard(authService);
    
      expect(authGuard.canActivate()).toEqual(false);
    });
  });
});
