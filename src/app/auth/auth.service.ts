import { Injectable } from '@angular/core';
import { Employees } from  '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  public register(userInfo: Employees){
    localStorage.setItem('ACCESS_TOKEN', userInfo.firstName);
  }

  public isRegistered(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  public logout(){
    localStorage.removeItem('ACCESS_TOKEN');
  }
}
