import { fakeAsync, TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

const mockEmplyeeData: any = {
  firstName: "Neha",
  lastName: "Goswami",
  gender: "female",
  dob: "1992-03-24",
  department: "UI developer"
}

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set token and return true from register', fakeAsync(() => {
    localStorage.setItem('ACCESS_TOKEN', "Neha");
    expect(service.register).toBeTruthy();
  }));

  it('should get token and return  true from isRegister', fakeAsync(() => {
    localStorage.getItem('ACCESS_TOKEN');
    expect(service.isRegistered()).toBeTruthy();
  }));

  it('should remove token and return false from isRegister', fakeAsync(() => {
    localStorage.removeItem('ACCESS_TOKEN');
    expect(service.isRegistered()).toBeFalsy();
  }));

  it('should set register and define register method', fakeAsync(() => {
    service.register(mockEmplyeeData);
    expect(service.register).toBeDefined()
  }));

  it('should remove token and define logout method', fakeAsync(() => {
    localStorage.removeItem('ACCESS_TOKEN')
    service.logout();
    expect(service.logout).toBeDefined()
  }));

});
